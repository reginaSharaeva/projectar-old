<?php

namespace App\Entity;

use App\Repository\TaskRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TaskRepository::class)
 */
class Task
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=1000)
     */
    private $name;

    /**
     * Связь с требованием n:1
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Requirement", inversedBy="tasks")
     */
    private $requirement;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    public function getRequirement(): ?Task
    {
        return $this->requirement;
    }

    public function setRequirement(Requirement $requirement): self
    {
        $this->requirement = $requirement;
        return $this;
    }
}
