<?php

namespace App\Entity;

use App\Repository\RequirementRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

/**
 * @ORM\Entity(repositoryClass=RequirementRepository::class)
 */
class Requirement
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=1000)
     */
    private $name;

    /**
     * Связь с проектом n:1
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Project", inversedBy="requirements")
     */
    private $project;

    /**
     * Связь с задачами 1:n
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Task", mappedBy="requirement")
     */
    private $tasks;

    public function __construct()
    {
        $this->tasks = new ArrayCollection();
    }

    /**
     * @return Collection|Task[]
     */
    public function getTasks(): Collection
    {
        return $this->tasks;
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    public function getProject(): ?Project
    {
        return $this->project;
    }

    public function setProject(Project $project): self
    {
        $this->project = $project;
        return $this;
    }
}
