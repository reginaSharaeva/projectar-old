<?php

namespace App\Controller;

use Symfony\Component\Security\Core\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class MainController extends AbstractController
{
    /**
     * @Route("/", name="app_main")
     */
    public function index(Security $security): Response
    {
        $user = $security->getUser();

        if ($user == null) {
            return $this->redirectToRoute('login');
        } else {
            return $this->redirectToRoute('app_profile');
        } 
    }
}    