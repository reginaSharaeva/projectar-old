<?php

namespace App\Controller;

use App\Entity\AccountSetting;
use App\Form\AccountSettingType;
use App\Repository\AccountSettingRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/account/setting")
 */
class AccountSettingController extends AbstractController
{
    /**
     * @Route("/", name="account_setting_index", methods={"GET"})
     */
    public function index(AccountSettingRepository $accountSettingRepository): Response
    {
        return $this->render('account_setting/index.html.twig', [
            'account_settings' => $accountSettingRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="account_setting_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $accountSetting = new AccountSetting();
        $form = $this->createForm(AccountSettingType::class, $accountSetting);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($accountSetting);
            $entityManager->flush();

            return $this->redirectToRoute('account_setting_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('account_setting/new.html.twig', [
            'account_setting' => $accountSetting,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="account_setting_show", methods={"GET"})
     */
    public function show(AccountSetting $accountSetting): Response
    {
        return $this->render('account_setting/show.html.twig', [
            'account_setting' => $accountSetting,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="account_setting_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, AccountSetting $accountSetting): Response
    {
        $form = $this->createForm(AccountSettingType::class, $accountSetting);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('account_setting_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('account_setting/edit.html.twig', [
            'account_setting' => $accountSetting,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="account_setting_delete", methods={"POST"})
     */
    public function delete(Request $request, AccountSetting $accountSetting): Response
    {
        if ($this->isCsrfTokenValid('delete'.$accountSetting->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($accountSetting);
            $entityManager->flush();
        }

        return $this->redirectToRoute('account_setting_index', [], Response::HTTP_SEE_OTHER);
    }
}
