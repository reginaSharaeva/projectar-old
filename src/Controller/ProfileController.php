<?php

namespace App\Controller;

use Symfony\Component\Security\Core\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\WorkspaceRepository;

class ProfileController extends AbstractController
{
    /**
     * @Route("/profile", name="app_profile")
     */
    public function enter(Security $security): Response
    {
        return $this->redirectToRoute('app_workspace'); 
    }

    /**
     * @Route("/workspace", name="app_workspace")
     */
    public function index(Security $security, WorkspaceRepository $workspaceRepository): Response
    {
    	$user = $security->getUser();
    	$workspaces = $workspaceRepository->findByUser($user->getId());
    	$workspace = $workspaces[0];
    	$projects = $workspace->getProjects();

        return $this->render('profile/index.html.twig', [
            'workspaces' => $workspaces,
            'workspace' => $workspace,
            'projects' => $projects
        ]);
    }


}    