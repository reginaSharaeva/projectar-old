<?php

namespace App\Controller;

use App\Entity\Workspace;
use App\Form\WorkspaceType;
use App\Repository\WorkspaceRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\ProjectRepository;

/**
 * @Route("/workspace")
 */
class WorkspaceController extends AbstractController
{

    /**
     * @Route("/{id}", name="workspace_show", methods={"GET"})
     */
    public function show(Workspace $workspace, ProjectRepository $projectRepository): Response
    {
        // $all_project = $projectRepository->findBy
        // return $this->redirectToRoute('app_profile_admin_author_all', ["child" => $child]);
    }
    
    /**
     * @Route("/", name="workspace_index", methods={"GET"})
     */
    public function index(WorkspaceRepository $workspaceRepository): Response
    {
        return $this->render('workspace/index.html.twig', [
            'workspaces' => $workspaceRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="workspace_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $workspace = new Workspace();
        $form = $this->createForm(WorkspaceType::class, $workspace);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($workspace);
            $entityManager->flush();

            return $this->redirectToRoute('workspace_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('workspace/new.html.twig', [
            'workspace' => $workspace,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="workspace_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Workspace $workspace): Response
    {
        $form = $this->createForm(WorkspaceType::class, $workspace);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('workspace_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('workspace/edit.html.twig', [
            'workspace' => $workspace,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="workspace_delete", methods={"POST"})
     */
    public function delete(Request $request, Workspace $workspace): Response
    {
        if ($this->isCsrfTokenValid('delete'.$workspace->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($workspace);
            $entityManager->flush();
        }

        return $this->redirectToRoute('workspace_index', [], Response::HTTP_SEE_OTHER);
    }
}
