<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211217213417 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE project ADD workspace_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE project ADD CONSTRAINT FK_2FB3D0EE82D40A1F FOREIGN KEY (workspace_id) REFERENCES workspace (id)');
        $this->addSql('CREATE INDEX IDX_2FB3D0EE82D40A1F ON project (workspace_id)');
        $this->addSql('ALTER TABLE workspace ADD user_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE workspace ADD CONSTRAINT FK_8D940019A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_8D940019A76ED395 ON workspace (user_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE project DROP FOREIGN KEY FK_2FB3D0EE82D40A1F');
        $this->addSql('DROP INDEX IDX_2FB3D0EE82D40A1F ON project');
        $this->addSql('ALTER TABLE project DROP workspace_id');
        $this->addSql('ALTER TABLE workspace DROP FOREIGN KEY FK_8D940019A76ED395');
        $this->addSql('DROP INDEX IDX_8D940019A76ED395 ON workspace');
        $this->addSql('ALTER TABLE workspace DROP user_id');
    }
}
