<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211217221746 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE requirement ADD project_id INT DEFAULT NULL, ADD name VARCHAR(1000) NOT NULL');
        $this->addSql('ALTER TABLE requirement ADD CONSTRAINT FK_DB3F5550166D1F9C FOREIGN KEY (project_id) REFERENCES project (id)');
        $this->addSql('CREATE INDEX IDX_DB3F5550166D1F9C ON requirement (project_id)');
        $this->addSql('ALTER TABLE task ADD requirement_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE task ADD CONSTRAINT FK_527EDB257B576F77 FOREIGN KEY (requirement_id) REFERENCES requirement (id)');
        $this->addSql('CREATE INDEX IDX_527EDB257B576F77 ON task (requirement_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE requirement DROP FOREIGN KEY FK_DB3F5550166D1F9C');
        $this->addSql('DROP INDEX IDX_DB3F5550166D1F9C ON requirement');
        $this->addSql('ALTER TABLE requirement DROP project_id, DROP name');
        $this->addSql('ALTER TABLE task DROP FOREIGN KEY FK_527EDB257B576F77');
        $this->addSql('DROP INDEX IDX_527EDB257B576F77 ON task');
        $this->addSql('ALTER TABLE task DROP requirement_id');
    }
}
